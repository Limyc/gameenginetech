#pragma once

#include "ostrich_types.h"
#include <math.h>

struct BitScanResult
{
	b32 isFound;
	u32 index;
};

inline f32 Abs(f32 x)
{
	f32 result = fabsf(x);
	return result;
}

inline u32 RotateLeft(u32 value, s32 amount)
{
#if COMPILER_MSVC
	u32 result = _rotl(value, amount);
#else
	amount &= 31;
	u32 result = (value << amount) | (value >> (32 - amount));
#endif
	return result;
}

inline u32 RotateRight(u32 value, s32 amount)
{
#if COMPILER_MSVC
	u32 result = _rotr(value, amount);
#else
	amount &= 31;
	u32 result = (value >> amount) | (value << (32 - amount));
#endif
	return result;
}

inline s32 RoundToInt32(f32 value)
{
	s32 result = (s32)roundf(value);
	return result;
}

inline u32 RoundToUInt32(f32 value)
{
	u32 result = (u32)roundf(value);
	return result;
}

inline s32 FloorToInt32(f32 value)
{
	s32 result = (s32)floorf(value);
	return result;
}

inline s32 CeilToInt32(f32 value)
{
	s32 result = (s32)ceilf(value);
	return result;
}

inline s32 TruncateToInt32(f32 value)
{
	s32 result = (s32)(value);
	return result;
}

inline s32 SignOf(s32 value)
{
	s32 result = (0 < value) - (value < 0);
	return result;
}

inline f32 Sin(f32 radians)
{
	f32 result = sinf(radians);
	return result;
}

inline f32 Cos(f32 radians)
{
	f32 result = cosf(radians);
	return result;
}

inline f32 Atan2(f32 y, f32 x)
{
	f32 result = atan2f(y, x);
	return result;
}

inline f32 Sqrt(f32 x)
{
	f32 result = sqrtf(x);
	return result;
}

inline f32 Clamp(f32 value, f32 min, f32 max)
{
	f32 result = value;

	if (result < min) { return min; }
	if (result > max) { return min; }

	return result;
}

inline f32 Clamp01(f32 value)
{
	f32 result = value;

	if (result < 0.0f) { return 0.0f; }
	if (result > 1.0f) { return 1.0f; }

	return result;
}

inline BitScanResult FindLeastSignificantBit(u32 value)
{
	BitScanResult result = {};
#if COMPILER_MSVC
	result.isFound = _BitScanForward((unsigned long *)&result.index, value);
#else
	for (u32 i = 0; i < 32; ++i)
	{
		if (value & (1 << i))
		{
			result.index = i;
			result.isFound = true;
			break;
		}
}
#endif

	return result;
}

