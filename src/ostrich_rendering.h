#pragma once

#include "ostrich_math_types.h"
#include "GL/gl3w.h"

#define MAX_BATCH_SIZE 10000;

struct GLUniforms
{
	GLuint viewProj;
	GLuint spriteTexture;
};

struct GLAttributes
{
	GLuint position;
	GLuint color;
	GLuint texCoord;
	GLuint spriteFrame;
	GLuint model;
};

enum struct CommandType
{
	LoadSpriteVao,
	LoadCircleVao,
	LoadRectVao,
	LoadSprite,
	DrawSpriteBatch,
	DrawCircleBatch,
	DrawRectBatch,
};

struct LoadSpriteVaoData
{
	GLuint *quadVbo;
	GLuint *indexVbo;
	GLuint *frameVbo;
	GLuint *matrixVbo;

	u32 quadsCount;
	GLfloat *quads;

	u32 indiciesCount;
	GLuint *indicies;
	
	u32 frameCountMax;
	u32 matrixCountMax;
};

struct LoadPrimativeVaoData
{
	GLuint *primativeVbo;
	GLuint *matrixVbo;

	u32 vertCount;
	GLfloat *verts;

	u32 matrixCountMax;
};

struct LoadSpriteData
{
	GLuint *textureID;
	s32 width;
	s32 height;
	u8 *pixels;
};

struct PrimativeBatchData
{
	u32 primativeCount;
	u32 primativeCountMax;
	mat4 *model;
};

struct SpriteBatchData
{
	u32 spriteCount;
	u32 spriteCountMax;
	v4 *spriteFrame;
	mat4 *model;
};

struct RenderCommand
{
	CommandType type;
	void *data;
};