#pragma once

#define WIN32_STATE_FILENAME_COUNT MAX_PATH

struct Win32GameCode
{
	HMODULE gameCodeDll;
	FILETIME dllLastWriteTime;

	// Either of the callbacks can be 0. You must check before calling
	GameSimulateFunc *Simulate;
	GameRenderFunc *Render;

	//GameGetSoundSamplesFunc *GetSoundSamples;

	b32 isValid;
};

struct Win32State
{
	u64 totalSize;
	void *gameMemoryBlock;
	//Win32ReplayBuffer ReplayBuffers[4];

	//HANDLE RecordingHandle;
	//s32 InputRecordingIndex;

	//HANDLE PlaybackHandle;
	//s32 InputPlaybackIndex;

	char exeFullPath[WIN32_STATE_FILENAME_COUNT];
	char *exeFileName;
};