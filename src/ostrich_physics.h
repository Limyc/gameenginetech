#pragma once

#include "ostrich_platform.h"
#include "ostrich_math_types.h"

const f32 k_correctionPercent = 0.2f;
const f32 k_slop = 0.01f;
const v2 k_gravity = { 0, -9.81f };

struct AABB
{
	v2 center;
	v2 extents;
};

struct Circle
{
	v2 center;
	f32 radius;
};

enum struct ShapeType
{
	Circle,
	Rect,
	Poly,
};

struct Shape
{
	ShapeType type;
	union
	{
		AABB aabb;
		Circle circle;
	};
};

struct MassData
{
	f32 mass;
	f32 invMass;
	f32 inertia;
	f32 invIntertia;
};

struct Body
{
	Shape shape;
	MassData massData;
	v2 position;
	v2 velocity;
	v2 force;
	f32 drag;
	f32 restitution;
	f32 gravityScale;
};

struct Manifold
{
	Body *a;
	Body *b;
	f32 penetration;
	v2 normal;
};