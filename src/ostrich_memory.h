#pragma once

#include "ostrich_types.h"

struct MemoryArena
{
	memIndex size;
	memIndex used;
	u8 *base;
};

internal void InitializeArena(MemoryArena *arena, memIndex size, u8 *base)
{
	arena->size = size;
	arena->base = base;
	arena->used = 0;
}

#define PushStruct(arena, type) (type *)PushSize_(arena, sizeof(type))
#define PushArray(arena, count, type) (type *)PushSize_(arena, (count) * sizeof(type))
#define PushSize(arena, size) PushSize_(arena, size)

internal void *PushSize_(MemoryArena *arena, memIndex size)
{
	//Assert((arena->used + size) <= arena->size);

	void *result = arena->base + arena->used;
	arena->used += size;

	return result;
}

internal void Clear(MemoryArena *arena)
{
	InitializeArena(arena, arena->size, arena->base);
}