#pragma once

#include "ostrich_platform.h"
#include "ostrich_math_types.h"
#include "ostrich_memory.h"
#include "ostrich_physics.h"

#define PIXELS_PER_METER 32

struct LoadedBitmap
{
	SDL_Texture *texture;
	s32 width;
	s32 height;
};

struct Sprite
{
	u8 *pixels;
	s32 width;
	s32 height;
	s32 numComponents;
};

enum struct EntityType
{
	Null,
	Player,
	Wall,
	Ball,
};

struct Animation
{
	b32 isFlipped;
	f64 nextFrameTime;
	f32 msPerFrame;
	u32 currentFrame;
	u32 framesCount;
	v4 *frames;
};

struct Camera
{
	Transform transform;
	mat4 projection;
	u32 clearFlags;
	v3 clearColor;
};

struct Entity
{
	EntityType type;
	Transform transform;
	Body body;
	Animation *animations;
	v2 size;
	b32 isFlipped;
};

struct GameState
{
	MemoryArena worldArena;

	u32 entityCount;
	Entity entities[256];

	u32 playerIndexForController[ArrayCount(((GameInput *)0)->controllers)];

	GameTimer spawnTimer;

	Camera camera;

	Sprite ostrichAtlas;
	Animation ostrichAnim;

	//SDL_Texture Shadow;
	//SDL_Texture HeroBitmaps[4];
};

