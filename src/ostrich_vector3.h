#pragma once

#include "ostrich_platform.h"
#include "ostrich_math_types.h"
#include "ostrich_intrinsics.h"

inline Vector3 operator-(const Vector3 &a)
{
	Vector3 result = { -a.x, -a.y, -a.z };
	return result;
}

inline Vector3 operator+(const Vector3 &a, const Vector3 &b)
{
	Vector3 result = { a.x + b.x, a.y + b.y, a.z + b.z };
	return result;
}

inline Vector3 operator-(const Vector3 &a, const Vector3 &b)
{
	Vector3 result = { a.x - b.x, a.y - b.y, a.z - b.z };
	return result;
}

inline Vector3 operator*(const Vector3 &a, f32 s)
{
	Vector3 result = { a.x * s, a.y * s, a.z * s };
	return result;
}

inline Vector3 operator*(f32 s, const Vector3 &a)
{
	Vector3 result = { a.x * s, a.y * s, a.z * s };
	return result;
}

inline Vector3 operator/(const Vector3 &a, f32 s)
{
	Assert(s != 0.0f);

	f32 inv = 1.0f / s;
	Vector3 result = { a.x * inv, a.y * inv, a.z * inv };
	return result;
}

inline Vector3 &operator+=(Vector3 &a, const Vector3 &b)
{
	a = a + b;
	return a;
}

inline Vector3 &operator-=(Vector3 &a, const Vector3 &b)
{
	a = a - b;
	return a;
}

inline Vector3 &operator*=(Vector3 &a, f32 s)
{
	a = a * s;
	return a;
}

inline Vector3 &operator/=(Vector3 &a, f32 s)
{
	a = a / s;
	return a;
}

inline b32 operator==(const Vector3 &a, const Vector3 &b)
{
	for (u32 i = 0; i < 3; ++i)
	{
		if (a.arr[i] != b.arr[i])
		{
			return false;
		}
	}

	return true;
}

inline f32 Length(const Vector3 &a)
{
	f32 result = Sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
	return result;
}

inline f32 LengthSqr(const Vector3 &a)
{
	f32 result = a.x * a.x + a.y * a.y + a.z * a.z;
	return result;
}

inline Vector3 Hadamard(const Vector3 &a, const Vector3 &b)
{
	Vector3 result = { a.x * b.x, a.y * b.y, a.z * b.z };
	return result;
}

inline f32 Dot(const Vector3 &a, const Vector3 &b)
{
	f32 result = a.x * b.x + a.y * b.y + a.z * b.z;
	return result;
}

inline Vector3 Cross(const Vector3 &a, const Vector3 &b)
{
	Vector3 result =
	{
		(a.y * b.z - b.y * a.z),
		(a.z * b.x - b.z * a.x),
		(a.x * b.y - b.x * a.y)
	};

	return result;
}

inline Vector3 Project(const Vector3 &a, const Vector3 &b)
{
	Vector3 result = (Dot(a, b) / Dot(b, b)) * b;
	return result;
}

inline Vector3 ProjectN(const Vector3 &a, const Vector3 &normal)
{
	Assert(LengthSqr(normal) == 1.0f || LengthSqr(normal) == 0.0f);

	Vector3 result = Dot(a, normal) * normal;
	return result;
}

inline Vector3 Reflect(const Vector3 &a, const Vector3 &b)
{
	Vector3 result = a - (2.0f * Project(a, b));
	return result;
}

inline Vector3 ReflectN(const Vector3 &a, const Vector3 &normal)
{
	Assert(LengthSqr(normal) == 1.0f || LengthSqr(normal) == 0.0f);

	Vector3 result = a - (2.0f * ProjectN(a, normal));
	return result;
}

inline Vector3 Normalized(const Vector3 &a)
{
	Assert(a.x != 0.0f || a.y != 0.0f || a.z != 0.0f);

	Vector3 result = a * (1.0f / Length(a));
	return result;
}
