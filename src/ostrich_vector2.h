#pragma once

#include "ostrich_platform.h"
#include "ostrich_math_types.h"
#include "ostrich_intrinsics.h"

inline Vector2 operator-(const Vector2 &a)
{
	Vector2 result = { -a.x, -a.y };
	return result;
}

inline Vector2 operator+(const Vector2 &a, const Vector2 &b)
{
	Vector2 result = { a.x + b.x, a.y + b.y };
	return result;
}

inline Vector2 operator-(const Vector2 &a, const Vector2 &b)
{
	Vector2 result = { a.x - b.x, a.y - b.y };
	return result;
}

inline Vector2 operator*(const Vector2 &a, f32 s)
{
	Vector2 result = { a.x * s, a.y * s };
	return result;
}

inline Vector2 operator*(f32 s, const Vector2 &a)
{
	Vector2 result = { a.x * s, a.y * s };
	return result;
}

inline Vector2 operator/(const Vector2 &a, f32 s)
{
	Assert(s != 0.0f);

	f32 inv = 1.0f / s;
	Vector2 result = { a.x * inv, a.y * inv };
	return result;
}

inline Vector2 &operator+=(Vector2 &a, const Vector2 &b)
{
	a = a + b;
	return a;
}

inline Vector2 &operator-=(Vector2 &a, const Vector2 &b)
{
	a = a - b;
	return a;
}

inline Vector2 &operator*=(Vector2 &a, f32 s)
{
	a = a * s;
	return a;
}

inline Vector2 &operator/=(Vector2 &a, f32 s)
{
	a = a / s;
	return a;
}

inline b32 &operator==(Vector2 &a, Vector2 &b)
{
	b32 result = (a.x == b.x) && (a.y == b.y);
	return result;
}

inline f32 Length(const Vector2 &a)
{
	f32 result = Sqrt(a.x * a.x + a.y * a.y);
	return result;
}

inline f32 LengthSqr(const Vector2 &a)
{
	f32 result = a.x * a.x + a.y * a.y;
	return result;
}

inline Vector2 Hadamard(const Vector2 &a, const Vector2 &b)
{
	Vector2 result = { a.x * b.x, a.y * b.y };
	return result;
}

inline f32 Dot(const Vector2 &a, const Vector2 &b)
{
	f32 result = a.x * b.x + a.y * b.y;
	return result;
}

inline f32 Cross(const Vector2 &a, const Vector2 &b)
{
	f32 result = a.x * b.y - a.y * b.x;
	return result;
}

inline Vector2 Perp(const Vector2 &a)
{
	Vector2 result = { a.y, -a.x };
	return result;
}

inline Vector2 Project(const Vector2 &a, const Vector2 &b)
{
	Vector2 result = (Dot(a, b) / Dot(b, b)) * b;
	return result;
}

inline Vector2 ProjectN(const Vector2 &a, const Vector2 &normal)
{
	Assert(LengthSqr(normal) == 1.0f || LengthSqr(normal) == 0.0f);

	Vector2 result = Dot(a, normal) * normal;
	return result;
}

inline Vector2 Reflect(const Vector2 &a, const Vector2 &b)
{
	Vector2 result = a - (2.0f * Project(a, b));
	return result;
}

inline Vector2 ReflectN(const Vector2 &a, const Vector2 &normal)
{
	Assert(LengthSqr(normal) == 1.0f || LengthSqr(normal) == 0.0f);

	Vector2 result = a - (2.0f * ProjectN(a, normal));
	return result;
}

inline Vector2 Normalized(const Vector2 &a)
{
	Assert(a.x != 0.0f || a.y != 0.0f);

	Vector2 result = a * (1.0f / Length(a));
	return result;
}

inline Vector2 Clamp(const Vector2 &value, const Vector2 &min, const Vector2 &max)
{
	Vector2 result = value;

	result.x = Clamp(result.x, min.x, max.x);
	result.y = Clamp(result.y, min.y, max.y);

	return result;
}
