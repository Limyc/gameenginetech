#pragma once

#include "ostrich_types.h"

union Vector2
{
	struct
	{
		f32 x;
		f32 y;
	};
	struct
	{
		f32 u;
		f32 v;
	};
	f32 arr[2];
};

typedef Vector2 v2;

union Vector3
{
	struct
	{
		f32 x;
		f32 y;
		f32 z;
	};
	struct
	{
		f32 r;
		f32 g;
		f32 b;
	};
	f32 arr[3];
};

typedef Vector3 v3;

union Vector4
{
	struct
	{
		f32 x;
		f32 y;
		f32 z;
		f32 w;
	};
	struct
	{
		f32 r;
		f32 g;
		f32 b;
		f32 a;
	};
	f32 arr[4];
};

typedef Vector4 v4;


union Matrix4
{
	struct
	{
		f32 m00;
		f32 m01;
		f32 m02;
		f32 m03;
		f32 m10;
		f32 m11;
		f32 m12;
		f32 m13;
		f32 m20;
		f32 m21;
		f32 m22;
		f32 m23;
		f32 m30;
		f32 m31;
		f32 m32;
		f32 m33;
	};
	struct
	{
		Vector4 row0;
		Vector4 row1;
		Vector4 row2;
		Vector4 row3;
	};
	f32	arr[16];
};

namespace matrix4
{
	const Matrix4 Identity =
	{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};
}

typedef Matrix4 mat4;

union Quaternion
{
	struct
	{
		f32 x;
		f32 y;
		f32 z;
		f32 w;
	};
	struct
	{
		v3 axis;
		f32 angle;
	};
	f32 arr[4];
};

struct Transform
{
	v2 position;
	f32 rotation;
	f32 scale;
};

struct Rect
{
	v2 min;
	v2 max;
};

struct Box
{
	v3 min;
	v3 max;
};