#include "ostrich_platform.h"
#include "ostrich_rendering.h"

internal void ExecuteRenderCommands(GameRenderBuffer *buffer, GLUniforms *uniforms, GLAttributes *attribs)
{
	RenderCommand *cmds = buffer->cmds;

	const v3 clearColor = { 53.0f / 255.0f, 53.0f / 255.0f, 73.0f / 255.0f };

	glClearColor(clearColor.r, clearColor.g, clearColor.b, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glUniformMatrix4fv(uniforms->viewProj, 1, GL_FALSE, buffer->viewProj.arr);

	for (u32 cmdIndex = 0; cmdIndex < buffer->cmdsCount; ++cmdIndex)
	{
		RenderCommand *cmd = &cmds[cmdIndex];

		switch (cmd->type)
		{
			case CommandType::LoadSpriteVao:
			{
				LoadSpriteVaoData *data = (LoadSpriteVaoData *)cmd->data;

				glGenBuffers(1, data->quadVbo);
				glGenBuffers(1, data->indexVbo);
				glGenBuffers(1, data->frameVbo);
				glGenBuffers(1, data->matrixVbo);

				glBindVertexArray(buffer->vaos[0]);

				glBindBuffer(GL_ARRAY_BUFFER, *data->quadVbo);
				glBufferData(GL_ARRAY_BUFFER, data->quadsCount * sizeof(GLfloat), data->quads, GL_STATIC_DRAW);

				glVertexAttribPointer(attribs->position, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid *)0);
				glEnableVertexAttribArray(attribs->position);

				glVertexAttribPointer(attribs->texCoord, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid *)(2 * sizeof(GLfloat)));
				glEnableVertexAttribArray(attribs->texCoord);

				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *data->indexVbo);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, data->indiciesCount * sizeof(GLuint), data->indicies, GL_STATIC_DRAW);

				glBindBuffer(GL_ARRAY_BUFFER, *data->frameVbo);
				glBufferData(GL_ARRAY_BUFFER, (data->frameCountMax * sizeof(v4)), 0, GL_STREAM_DRAW);

				glVertexAttribPointer(attribs->spriteFrame, 4, GL_FLOAT, GL_FALSE, sizeof(v4), (GLvoid *)0);
				glVertexAttribDivisor(attribs->spriteFrame, 1);
				glEnableVertexAttribArray(attribs->spriteFrame);

				glBindBuffer(GL_ARRAY_BUFFER, *data->matrixVbo);
				glBufferData(GL_ARRAY_BUFFER, (data->matrixCountMax * sizeof(mat4)), 0, GL_STREAM_DRAW);

				for (u32 i = 0; i < 4; ++i)
				{
					GLuint loc = attribs->model + i;
					glVertexAttribPointer(loc, 4, GL_FLOAT, GL_FALSE, sizeof(mat4), (GLvoid *)(i * sizeof(v4)));
					glVertexAttribDivisor(loc, 1);
					glEnableVertexAttribArray(loc);
				}

				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);
			} break;
			case CommandType::LoadCircleVao:
			{
				LoadPrimativeVaoData *data = (LoadPrimativeVaoData *)cmd->data;

				glGenBuffers(1, data->primativeVbo);
				glGenBuffers(1, data->matrixVbo);

				glBindVertexArray(buffer->vaos[1]);

				glBindBuffer(GL_ARRAY_BUFFER, *data->primativeVbo);
				glBufferData(GL_ARRAY_BUFFER, data->vertCount * sizeof(GLfloat), data->verts, GL_STATIC_DRAW);

				glVertexAttribPointer(attribs->position, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid *)0);
				glEnableVertexAttribArray(attribs->position);

				glBindBuffer(GL_ARRAY_BUFFER, *data->matrixVbo);
				glBufferData(GL_ARRAY_BUFFER, (data->matrixCountMax * sizeof(mat4)), 0, GL_STREAM_DRAW);

				for (u32 i = 0; i < 4; ++i)
				{
					GLuint loc = attribs->model + i;
					glVertexAttribPointer(loc, 4, GL_FLOAT, GL_FALSE, sizeof(mat4), (GLvoid *)(i * sizeof(v4)));
					glVertexAttribDivisor(loc, 1);
					glEnableVertexAttribArray(loc);
				}

				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);
			} break;
			case CommandType::LoadRectVao:
			{
				LoadPrimativeVaoData *data = (LoadPrimativeVaoData *)cmd->data;

				glGenBuffers(1, data->primativeVbo);
				glGenBuffers(1, data->matrixVbo);

				glBindVertexArray(buffer->vaos[2]);

				glBindBuffer(GL_ARRAY_BUFFER, *data->primativeVbo);
				glBufferData(GL_ARRAY_BUFFER, data->vertCount * sizeof(GLfloat), data->verts, GL_STATIC_DRAW);

				glVertexAttribPointer(attribs->position, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid *)0);
				glEnableVertexAttribArray(attribs->position);

				glBindBuffer(GL_ARRAY_BUFFER, *data->matrixVbo);
				glBufferData(GL_ARRAY_BUFFER, (data->matrixCountMax * sizeof(mat4)), 0, GL_STREAM_DRAW);

				for (u32 i = 0; i < 4; ++i)
				{
					GLuint loc = attribs->model + i;
					glVertexAttribPointer(loc, 4, GL_FLOAT, GL_FALSE, sizeof(mat4), (GLvoid *)(i * sizeof(v4)));
					glVertexAttribDivisor(loc, 1);
					glEnableVertexAttribArray(loc);
				}

				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);
			} break;
			case CommandType::LoadSprite:
			{
				LoadSpriteData *data = (LoadSpriteData *)cmd->data;

				glGenTextures(1, data->textureID);
				glBindTexture(GL_TEXTURE_2D, *data->textureID);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, data->width, data->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data->pixels);

				glBindTexture(GL_TEXTURE_2D, 0);

			} break;
			case CommandType::DrawSpriteBatch:
			{
				SpriteBatchData *data = (SpriteBatchData *)cmd->data;

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, buffer->tex1);

				glUseProgram(buffer->spriteShader);

				glUniform1i(uniforms->spriteTexture, 0);

				glBindBuffer(GL_ARRAY_BUFFER, buffer->vbos[2]);
				GLintptr *ptr = (GLintptr *)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
				Assert(ptr);
				memcpy(ptr, data->spriteFrame, data->spriteCount * sizeof(v4));

				u32 mapSuccess = glUnmapBuffer(GL_ARRAY_BUFFER);
				Assert(mapSuccess);

				glBindBuffer(GL_ARRAY_BUFFER, buffer->vbos[3]);
				ptr = (GLintptr *)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
				Assert(ptr);
				memcpy(ptr, data->model, data->spriteCount * sizeof(mat4));

				mapSuccess = glUnmapBuffer(GL_ARRAY_BUFFER);
				Assert(mapSuccess);

				glBindVertexArray(buffer->vaos[0]);
				glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0, data->spriteCount);
				glBindVertexArray(0);
			} break;
			case CommandType::DrawCircleBatch:
			{
				PrimativeBatchData *data = (PrimativeBatchData*)cmd->data;

				glUseProgram(buffer->primativeShader);

				glBindBuffer(GL_ARRAY_BUFFER, buffer->vbos[5]);
				GLintptr *ptr = (GLintptr *)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
				Assert(ptr);
				memcpy(ptr, data->model, data->primativeCount * sizeof(mat4));

				b32 mapSuccess = glUnmapBuffer(GL_ARRAY_BUFFER);
				Assert(mapSuccess);

				glBindVertexArray(buffer->vaos[1]);
				glDrawArraysInstanced(GL_LINE_LOOP, 0, 120, data->primativeCount);
				glBindVertexArray(0);


			} break;
			case CommandType::DrawRectBatch:
			{
				PrimativeBatchData *data = (PrimativeBatchData*)cmd->data;

				glUseProgram(buffer->primativeShader);

				glBindBuffer(GL_ARRAY_BUFFER, buffer->vbos[7]);
				GLintptr *ptr = (GLintptr *)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
				Assert(ptr);
				memcpy(ptr, data->model, data->primativeCount * sizeof(mat4));

				b32 mapSuccess = glUnmapBuffer(GL_ARRAY_BUFFER);
				Assert(mapSuccess);

				glBindVertexArray(buffer->vaos[2]);
				glDrawArraysInstanced(GL_LINE_LOOP, 0, 4, data->primativeCount);
				glBindVertexArray(0);


			} break;

		}

		GLenum error = glGetError();

		Assert(error == 0);
	}
}