#pragma once
#include "ostrich_types.h"
#include "ostrich_math_types.h"
#include "GL/gl3w.h"
#include "SDL2/SDL.h"
#include "ostrich_memory.h"
#include "ostrich_rendering.h"
#if OKG_SLOW
#define Assert(expression) \
	do { \
		if(!(expression)) { *(int *)0 = 0; } \
	} while(0);
#else
#define Assert(expression) {}
#endif

#define Kilobytes(value) ((value) * 1024LL)
#define Megabytes(value) (Kilobytes((value)) * 1024LL)
#define Gigabytes(value) (Megabytes((value)) * 1024LL)
#define Terabytes(value) (Gigabytes((value)) * 1024LL)

#define ArrayCount(array) (sizeof((array)) / sizeof((array)[0]))

#define PI32 3.14159265358979323846f
#define TAU32 6.28318530717958647692f

#define MAX_GAME_PADS 4

#define Minimum(a, b) ((a < b) ? (a) : (b))
#define Maximum(a, b) ((a > b) ? (a) : (b))

struct GameRenderBuffer
{
	GLuint vaos[3];
	GLuint vbos[10];
	GLuint tex1;
	GLuint spriteShader;
	GLuint primativeShader;
	mat4 viewProj;
	s32 width;
	s32 height;
	u32 cmdsCount;
	RenderCommand cmds[50];
	MemoryArena arena;
	b32 isInitialized;
};

struct GameButtonState
{
	s32 halfTransitionCount;
	b32 endedDown;
	b32 wasDown;
};

struct GameControllerInput
{
	b32 isConnected;
	b32 isAnalog;

	f32 lStickAverageX;
	f32 lStickAverageY;

	f32 rStickAverageX;
	f32 rStickAverageY;

	f32 lTrigger;

	f32 rTrigger;

	union
	{
		GameButtonState Buttons[12];
		struct
		{
			GameButtonState moveUp;
			GameButtonState moveDown;
			GameButtonState moveLeft;
			GameButtonState moveRight;

			GameButtonState actionUp;
			GameButtonState actionDown;
			GameButtonState actionLeft;
			GameButtonState actionRight;

			GameButtonState leftShoulder;
			GameButtonState rightShoulder;

			GameButtonState back;
			GameButtonState start;

			// Add buttons above this line

			GameButtonState terminator;
		};
	};
};

struct GameTimer
{
	s64 startTicks;
	s64 pausedTicks;
};

struct GameInput
{
	GameButtonState mouseButtons[MAX_GAME_PADS + 1];
	s32 mouseX;
	s32 mouseY;
	s32 mouseZ;

	f32 fixedDeltaTime;
	f32 deltaTime;
	f64 gameRealTime;
	f64 gameTime;

	GameControllerInput controllers[MAX_GAME_PADS + 1];
};

#define PLATFORM_LOAD_PNG(name) u8 *name(char *fileName, s32 *width, s32 *height, s32 *numComponents)
typedef PLATFORM_LOAD_PNG(PlatformLoadPngFunc);

#define PLATFORM_UNLOAD_PNG(name) void name(u8 *pixels)
typedef PLATFORM_UNLOAD_PNG(PlatformUnloadPngFunc);

struct PlatformApi
{
	PlatformLoadPngFunc *LoadPng;
	PlatformUnloadPngFunc *UnloadPng;
};

struct GameMemory
{
	b32 isInitialized;

	MemoryArena permStorage;
	MemoryArena tempStorage;

	//u64 permanentStorageSize;
	//void *permanentStorage;

	//u64 transientStorageSize;
	//void *transientStorage;

	PlatformApi platformApi;
	//DEBUGPlatformFreeFileMemoryFunc *DEBUGPlatformFreeFileMemory;
	//DEBUGPlatformReadEntireFileFunc *DEBUGPlatformReadEntireFile;
	//DEBUGPlatformWriteEntireFileFunc *DEBUGPlatformWriteEntireFile;
};

inline GameControllerInput *GetController(GameInput *input, s32 controllerIndex)
{
	Assert(controllerIndex < ArrayCount(input->controllers));

	GameControllerInput *result = &input->controllers[controllerIndex];

	return result;
}

inline s64 GetTicksElapsed(GameTimer *timer, s64 currentTicks)
{
	s64 result = currentTicks;

	if (timer->startTicks)
	{
		return result - timer->startTicks;
	}

	if (timer->pausedTicks)
	{
		return result - timer->pausedTicks;
	}

	return result;
}

inline void StartGameTimer(GameTimer *timer, s64 currentTicks)
{
	timer->startTicks = currentTicks;
	timer->pausedTicks = 0;
}

inline void PauseGameTimer(GameTimer *timer, s64 currentTicks)
{
	if (!timer->pausedTicks)
	{
		timer->pausedTicks = currentTicks - timer->startTicks;
		timer->startTicks = 0;
	}
}

inline void UnpauseGameTimer(GameTimer *timer, s64 currentTicks)
{
	if (timer->pausedTicks)
	{
		timer->startTicks = currentTicks - timer->pausedTicks;
		timer->pausedTicks = 0;
	}
}

inline void ResetGameTimer(GameTimer *timer)
{
	timer->startTicks = 0;
	timer->pausedTicks = 0;
}

#define GAME_SIMULATE(name) void name(GameMemory *memory, GameInput *input)
typedef GAME_SIMULATE(GameSimulateFunc);

#define GAME_RENDER(name) void name(GameMemory *memory, GameRenderBuffer *renderBuffer, GameInput *input)
typedef GAME_RENDER(GameRenderFunc);