#include "ostrich_platform.h"

#include "gl3w.c"

#define SDL_MAIN_HANDLED
#include "SDL2/SDL_main.h"
#include "SDL2/SDL_syswm.h"
#include "SDL2/SDL_joystick.h"
#include "SDL2/SDL_gamecontroller.h"
#include "SDL2/SDL_keyboard.h"
#include "SDL2/SDL_keycode.h"

#include <mmsystem.h>
#include <stdio.h>

#undef NEAR
#undef near
#undef FAR
#undef far

#include "win32_ostrich.h"
#include "ostrich_math_types.h"
#include "ostrich_matrix.h"
#include "ostrich_vector2.h"
#include "ostrich_opengl.cpp"

#pragma warning(push)
#pragma warning(disable: 244)
#pragma warning(disable: 4459)
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#pragma warning(pop)

globalVar GameRenderBuffer g_renderBuffer = {};
globalVar GLUniforms g_uniforms = {};
globalVar GLAttributes g_attribs = {};
globalVar SDL_Window *g_window = 0;
globalVar SDL_GLContext g_glContext = 0;
globalVar u32 g_sdlControllerCount = 0;
globalVar SDL_GameController *g_sdlControllers[4];
globalVar s64 g_perfCounterFrequency;

globalVar b32 g_running = false;
globalVar b32 g_pause = false;
globalVar b32 g_stepFrame = false;
globalVar b32 g_vsync = 1;
globalVar char *g_windowTitle = "OpenGL Test";

//globalVar GLuint renderBuffer->spriteShader;
globalVar b32 g_renderFrame = false;
globalVar GLenum g_drawType = GL_LINE_LOOP;

internal void CatStrings(size_t sourceACount, char *sourceA,
						 size_t sourceBCount, char *sourceB,
						 size_t destCount, char *dest)
{
	Assert(sourceACount + sourceBCount < destCount);

	for (u32 i = 0; i < sourceACount; ++i)
	{
		*dest++ = *sourceA++;
	}

	for (s32 i = 0; i < sourceBCount; ++i)
	{
		*dest++ = *sourceB++;
	}

	*dest++ = 0;
}

internal s32 StringLength(char *str)
{
	s32 count = 0;

	while (*str++)
	{
		++count;
	}

	return count;
}

internal b32 Win32InitGLAttributes()
{
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	return true;
}

internal b32 Win32InitSDLVideo(s32 width, s32 height)
{
	b32 result = false;

	if (SDL_Init(SDL_INIT_VIDEO) >= 0)
	{
		g_window = SDL_CreateWindow(g_windowTitle, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
									width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI);

		if (g_window)
		{
			g_glContext = SDL_GL_CreateContext(g_window);

			Win32InitGLAttributes();

			if (g_vsync)
			{
				SDL_GL_SetSwapInterval(1);
			}

			gl3wInit();

			result = true;
		}
		else
		{
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Window could not be created! SDL_Error: %s\n", SDL_GetError());
		}
	}
	else
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	}

	return result;
}

internal GLuint Win32LoadShader(char *path, GLenum shaderType)
{
	SDL_RWops *file = SDL_RWFromFile(path, "r");

	if (file)
	{
		s64 bufferSize = SDL_RWsize(file);

		char *src = (char *)calloc(1, bufferSize + 1);

		s64 readTotal = 0;
		s64 read = 1;
		char *arena = src;

		while (readTotal < bufferSize && read)
		{
			read = SDL_RWread(file, arena, 1, (bufferSize - readTotal));
			readTotal += read;
			arena += read;
		}

		SDL_RWclose(file);

		src[readTotal] = '\0';

		GLuint shader = glCreateShader(shaderType);
		glShaderSource(shader, 1, &src, (GLint *)&readTotal);
		glCompileShader(shader);

		b32 wasCompiled = false;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &wasCompiled);

		free(src);

		if (wasCompiled)
		{
			return shader;
		}
	}

	return 0;
}

internal GLuint Win32LoadVertexShader(char *path)
{
	return Win32LoadShader(path, GL_VERTEX_SHADER);
}

internal GLuint Win32LoadFragmentShader(char *path)
{
	return Win32LoadShader(path, GL_FRAGMENT_SHADER);
}

internal b32 Win32LinkShaders(GLuint shaderProgram)
{
	glLinkProgram(shaderProgram);

	b32 isLinked = false;

	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &isLinked);


	return isLinked;
}

internal b32 Win32InitShader(GLuint *shader, char *vertPath, char *fragPath)
{
	*shader = glCreateProgram();

	GLuint vert = Win32LoadVertexShader(vertPath);
	if (!vert) { return false; }

	GLuint frag = Win32LoadFragmentShader(fragPath);
	if (!frag) { return false; }

	glAttachShader(*shader, vert);
	glAttachShader(*shader, frag);

	b32 result = Win32LinkShaders(*shader);

	glDetachShader(*shader, vert);
	glDetachShader(*shader, frag);

	glDeleteShader(vert);
	glDeleteShader(frag);

	return result;
}

internal void Win32CheckGLError()
{
	GLenum err = glGetError();

	while (err != GL_NO_ERROR)
	{
		char *str;

		switch (err)
		{
			case GL_INVALID_OPERATION: str = "GL_INVALID_OPERATION"; break;
			case GL_INVALID_ENUM: str = "GL_INVALID_ENUM"; break;
			case GL_INVALID_VALUE: str = "GL_INVALID_VALUE"; break;
			case GL_OUT_OF_MEMORY: str = "GL_OUT_OF_MEMORY"; break;
			case GL_INVALID_FRAMEBUFFER_OPERATION: str = "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
			default: str = "UNKNOWN"; break;
		}

		OutputDebugStringA(str);

		err = glGetError();
	}

	Assert(err == GL_NO_ERROR);
}

internal void Win32GetExeFileName(Win32State *state)
{
	DWORD sizeOfPath = GetModuleFileNameA(0, state->exeFullPath, sizeof(state->exeFullPath));

	state->exeFileName = state->exeFullPath;

	for (char *scan = state->exeFullPath; *scan; ++scan)
	{
		if (*scan == '\\')
		{
			state->exeFileName = scan + 1;
		}
	}
}

internal void Win32BuildPathFromExeDirectory(Win32State *state, char *fileName, s32 destCount, char *dest)
{
	CatStrings(state->exeFileName - state->exeFullPath, state->exeFullPath,
			   StringLength(fileName), fileName,
			   destCount, dest);
}

internal FILETIME Win32GetLastWriteTime(char *fileName)
{
	FILETIME result = {};
	WIN32_FILE_ATTRIBUTE_DATA data;

	if (GetFileAttributesExA(fileName, GetFileExInfoStandard, &data))
	{
		result = data.ftLastWriteTime;
	}

	return result;
}

internal Win32GameCode Win32LoadGameCode(char *sourceDllName, char *tempDllName, char *lockFileName)
{
	Win32GameCode result = {};
	WIN32_FILE_ATTRIBUTE_DATA ignored;

	if (!GetFileAttributesEx(lockFileName, GetFileExInfoStandard, &ignored))
	{
		result.dllLastWriteTime = Win32GetLastWriteTime(sourceDllName);

		CopyFile(sourceDllName, tempDllName, false);
		result.gameCodeDll = LoadLibraryA(tempDllName);

		if (result.gameCodeDll)
		{
			result.Simulate = (GameSimulateFunc *)GetProcAddress(result.gameCodeDll, "GameSimulate");
			result.Render = (GameRenderFunc *)GetProcAddress(result.gameCodeDll, "GameRender");

			result.isValid = (result.Simulate && result.Render);
		}
	}

	if (!result.isValid)
	{
		result.Simulate = 0;
		result.Render = 0;
	}

	return result;
}

internal void Win32UnloadGameCode(Win32GameCode *gameCode)
{
	if (gameCode->isValid)
	{
		FreeLibrary(gameCode->gameCodeDll);
		gameCode->gameCodeDll = 0;
	}

	gameCode->isValid = false;
	gameCode->Simulate = 0;
	gameCode->Render = 0;
}

internal void Win32ProcessKeyboardMessage(GameButtonState *newState, b32 isDown)
{
	if (newState->endedDown != isDown)
	{
		newState->endedDown = isDown;
		++newState->halfTransitionCount;
	}
	else
	{
		newState->wasDown = isDown;
	}
}

internal void Win32ProcessSDLEvents(GameControllerInput *controller)
{
	SDL_Event event;

	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
		{
			g_running = false;
		}

		if (event.type == SDL_KEYDOWN || event.type == SDL_KEYUP)
		{
			SDL_KeyboardEvent keyEvent = event.key;

			b32 isDown = keyEvent.state == SDL_PRESSED;

			switch (keyEvent.keysym.sym)
			{
				case SDLK_ESCAPE:
				{
					g_running = false;
				} break;
				case SDLK_w:
				{
					Win32ProcessKeyboardMessage(&controller->moveUp, isDown);
				} break;
				case SDLK_s:
				{
					Win32ProcessKeyboardMessage(&controller->moveDown, isDown);
				} break;
				case SDLK_a:
				{
					Win32ProcessKeyboardMessage(&controller->moveLeft, isDown);
				} break;
				case SDLK_d:
				{
					Win32ProcessKeyboardMessage(&controller->moveRight, isDown);
				} break;
				default:
					break;
			}
		}
	}
}

u8 *Win32LoadPng(char *fileName, s32 *width, s32 *height, s32 *numComponents)
{
	u8 *result = stbi_load(fileName, width, height, numComponents, STBI_rgb_alpha);

	return result;
}

void Win32UnloadPng(u8 *pixels)
{
	stbi_image_free(pixels);
	*pixels = 0;
}

s32 CALLBACK WinMain(HINSTANCE instance, HINSTANCE prevInstance, LPSTR commandLine, s32 showCode)
{
	Win32State win32State = {};


	Win32GetExeFileName(&win32State);

	char sourceGameCodeDllPath[WIN32_STATE_FILENAME_COUNT];
	Win32BuildPathFromExeDirectory(&win32State, "ostrich.dll", sizeof(sourceGameCodeDllPath), sourceGameCodeDllPath);

	char tempGameCodeDllPath[WIN32_STATE_FILENAME_COUNT];
	Win32BuildPathFromExeDirectory(&win32State, "ostrich_temp.dll", sizeof(tempGameCodeDllPath), tempGameCodeDllPath);

	char gameCodeLockPath[WIN32_STATE_FILENAME_COUNT];
	Win32BuildPathFromExeDirectory(&win32State, "lock.tmp", sizeof(gameCodeLockPath), gameCodeLockPath);


	v3 clearColor = { 53.0f / 255.0f, 53.0f / 255.0f, 73.0f / 255.0f };
	v2 screenRes = { 1280, 720 };

	//if (Win32InitSDLVideo(128, 512))
	if (Win32InitSDLVideo((s32)screenRes.x, (s32)screenRes.y))
	{
		memIndex permanentStorageSize = Megabytes(256);
		memIndex transientStorageSize = Megabytes(64);

		LPVOID baseAddress = (LPVOID)Terabytes(2);

		win32State.totalSize = permanentStorageSize + transientStorageSize;
		win32State.gameMemoryBlock = VirtualAlloc(baseAddress, (SIZE_T)win32State.totalSize,
												  MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

		GameMemory gameMemory = {};
		gameMemory.platformApi.LoadPng = Win32LoadPng;
		gameMemory.platformApi.UnloadPng = Win32UnloadPng;
		//gameMemory.DEBUGPlatformFreeFileMemory = DEBUGPlatformFreeFileMemory;
		//gameMemory.DEBUGPlatformReadEntireFile = DEBUGPlatformReadEntireFile;
		//gameMemory.DEBUGPlatformWriteEntireFile = DEBUGPlatformWriteEntireFile;
		InitializeArena(&gameMemory.permStorage, permanentStorageSize, (u8 *)win32State.gameMemoryBlock);
		InitializeArena(&gameMemory.tempStorage, transientStorageSize, gameMemory.permStorage.base + gameMemory.permStorage.size);

		GameInput gameInput[2] = {};
		GameInput *oldInput = &gameInput[0];
		GameInput *newInput = &gameInput[1];

		if (Win32InitShader(&g_renderBuffer.spriteShader, "../shaders/sprite.vert", "../shaders/sprite.frag")
			&& Win32InitShader(&g_renderBuffer.primativeShader, "../shaders/primative.vert", "../shaders/primative.frag"))
		{
			g_running = true;

			Win32GameCode gameCode = Win32LoadGameCode(sourceGameCodeDllPath, tempGameCodeDllPath, gameCodeLockPath);

			f64 newTime = SDL_GetTicks();
			f64 currentTime = 0;
			f64 elapsedTime = 0;
			f64 frameTime = 0;
			f64 accumulator = 0;
			f32 dt = 1 / 120.0f;

			glGenVertexArrays(3, g_renderBuffer.vaos);

			glUseProgram(g_renderBuffer.spriteShader);
			g_uniforms.viewProj = glGetUniformLocation(g_renderBuffer.spriteShader, "viewProj");
			//g_uniforms.spriteFrame = glGetUniformLocation(g_renderBuffer.spriteShader, "spriteFrame");
			g_uniforms.spriteTexture = glGetUniformLocation(g_renderBuffer.spriteShader, "spriteTexture");

			g_attribs.position = 0;
			g_attribs.color = 1;
			g_attribs.texCoord = 2;
			g_attribs.spriteFrame = 3;
			g_attribs.model = 4;



			while (g_running)
			{
				newTime = SDL_GetTicks() * 0.001;
				frameTime = newTime - currentTime;
				currentTime = newTime;

				accumulator += frameTime;

				FILETIME newDllWriteTime = Win32GetLastWriteTime(sourceGameCodeDllPath);

				u32 count = 0;
				if (CompareFileTime(&newDllWriteTime, &gameCode.dllLastWriteTime) != 0)
				{
					Win32UnloadGameCode(&gameCode);
					for (u32 tryIndex = 0; !gameCode.isValid && (tryIndex < 100); ++tryIndex)
					{
						gameCode = Win32LoadGameCode(sourceGameCodeDllPath, tempGameCodeDllPath, gameCodeLockPath);

						if (!gameCode.isValid)
						{
							++count;
							Sleep(10);
						}
					}

					count = count;
				}

				Assert(gameCode.Simulate);
				Assert(gameCode.Render);

				GameControllerInput *oldKB = &oldInput->controllers[0];
				GameControllerInput *newKB = &newInput->controllers[0];
				*newKB = {};

				newKB->isConnected = true;

				for (u32 i = 0; i < ArrayCount(newKB->Buttons); ++i)
				{
					newKB->Buttons[i].endedDown = oldKB->Buttons[i].endedDown;
				}

				for (u32 i = 0; i < ArrayCount(newInput->mouseButtons); ++i)
				{
					newInput->mouseButtons[i].endedDown = oldInput->mouseButtons[i].endedDown;
					newInput->mouseButtons[i].wasDown = oldInput->mouseButtons[i].endedDown;
				}

				newInput->deltaTime = (f32)frameTime;
				newInput->gameTime = currentTime;

				while (accumulator >= dt)
				{
					Win32ProcessSDLEvents(newKB);

					SDL_GetMouseState(&newInput->mouseX, &newInput->mouseY);

					Win32ProcessKeyboardMessage(&newInput->mouseButtons[0], GetKeyState(VK_LBUTTON) & (1 << 15));
					Win32ProcessKeyboardMessage(&newInput->mouseButtons[2], GetKeyState(VK_MBUTTON) & (1 << 15));
					Win32ProcessKeyboardMessage(&newInput->mouseButtons[1], GetKeyState(VK_RBUTTON) & (1 << 15));
					Win32ProcessKeyboardMessage(&newInput->mouseButtons[3], GetKeyState(VK_XBUTTON1) & (1 << 15));
					Win32ProcessKeyboardMessage(&newInput->mouseButtons[4], GetKeyState(VK_XBUTTON2) & (1 << 15));

					newInput->fixedDeltaTime = dt;
					gameCode.Simulate(&gameMemory, newInput);

					accumulator -= dt;
					elapsedTime += dt;
				}

				gameCode.Render(&gameMemory, &g_renderBuffer, newInput);

				ExecuteRenderCommands(&g_renderBuffer, &g_uniforms, &g_attribs);

				Win32CheckGLError();

				SDL_GL_SwapWindow(g_window);

				GameInput *temp = newInput;
				newInput = oldInput;
				oldInput = temp;
			}
		}
		else
		{
			const s32 size = Kilobytes(1);
			char shaderInfoLog[size];
			glGetProgramInfoLog(g_renderBuffer.spriteShader, size, (GLsizei *)&size, (GLchar *)&shaderInfoLog);

			OutputDebugString(shaderInfoLog);
		}

		glDisableVertexAttribArray(0);

		for (u32 i = 0; i < ArrayCount(g_renderBuffer.vbos); i++)
		{
			glDeleteBuffers(1, &g_renderBuffer.vbos[i]);
		}

		glDeleteVertexArrays(3, g_renderBuffer.vaos);
		glUseProgram(0);

		glDeleteProgram(g_renderBuffer.spriteShader);

	}
	else
	{

	}

	SDL_GL_DeleteContext(g_glContext);
	SDL_DestroyWindow(g_window);
	SDL_Quit();

	return 0;
}