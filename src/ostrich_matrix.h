#pragma once

#include "ostrich_types.h"
#include "ostrich_intrinsics.h"
#include "ostrich_vector3.h"

inline Vector3 operator*(const Vector3 &vec, const Matrix4 &mat)
{
	Vector3 result = {};

	result.x = vec.x * mat.m00 + vec.y * mat.m10 + vec.z * mat.m20 + mat.m30;
	result.y = vec.x * mat.m01 + vec.y * mat.m11 + vec.z * mat.m21 + mat.m31;
	result.z = vec.x * mat.m02 + vec.y * mat.m12 + vec.z * mat.m22 + mat.m32;

	return result;
}

//// Pre-multiply
//inline Vector4 operator*(const Matrix4 &mat, const Vector4 &vec)
//{
//	Vector4 result = {};
//
//	result.x = vec.x * mat.m00 + vec.y * mat.m01 + vec.z * mat.m02 + vec.w * mat.m03;
//	result.y = vec.x * mat.m10 + vec.y * mat.m11 + vec.z * mat.m12 + vec.w * mat.m13;
//	result.z = vec.x * mat.m20 + vec.y * mat.m21 + vec.z * mat.m22 + vec.w * mat.m23;
//	result.w = vec.x * mat.m30 + vec.y * mat.m31 + vec.z * mat.m32 + vec.w * mat.m33;
//
//	return result;
//}

// Post-multiply
inline Vector4 operator*(const Vector4 &vec, const Matrix4 &mat)
{
	Vector4 result = {};

	result.x = vec.x * mat.m00 + vec.y * mat.m10 + vec.z * mat.m20 + vec.w * mat.m30;
	result.y = vec.x * mat.m01 + vec.y * mat.m11 + vec.z * mat.m21 + vec.w * mat.m31;
	result.z = vec.x * mat.m02 + vec.y * mat.m12 + vec.z * mat.m22 + vec.w * mat.m32;
	result.w = vec.x * mat.m03 + vec.y * mat.m13 + vec.z * mat.m23 + vec.w * mat.m33;

	return result;
}

inline Matrix4 operator*(const Matrix4 &a, const Matrix4 &b)
{
	Matrix4 result = {};

	result.row0 = a.row0 * b;
	result.row1 = a.row1 * b;
	result.row2 = a.row2 * b;
	result.row3 = a.row3 * b;

	return result;
}

inline b32 operator==(const Matrix4 &a, const Matrix4 &b)
{
	for (u32 i = 0; i < 16; ++i)
	{
		if (a.arr[i] != b.arr[i])
		{
			return false;
		}
	}

	return true;
}

inline Matrix4 Translate(const Vector3 &pos)
{
	Matrix4 result = matrix4::Identity;

	result.m30 = pos.x;
	result.m31 = pos.y;
	result.m32 = pos.z;

	return result;
}

inline Matrix4 Translate(const Vector2 &pos)
{
	Matrix4 result = matrix4::Identity;

	result.m30 = pos.x;
	result.m31 = pos.y;
	result.m32 = 0;

	return result;
}

inline Matrix4 RotateXYZ(const Vector3 &rot)
{
	Matrix4 result = {};

	f32 sx = Sin(rot.x);
	f32 cx = Cos(rot.x);
	f32 sy = Sin(rot.y);
	f32 cy = Cos(rot.y);
	f32 sz = Sin(rot.z);
	f32 cz = Cos(rot.z);

	result.m00 = cy * cz;
	result.m01 = -cy * sz;
}

inline Matrix4 RotateXYZ(const Vector2 &rot)
{
	Matrix4 result = {};

	f32 sx = Sin(rot.x);
	f32 cx = Cos(rot.x);
	f32 sy = Sin(rot.y);
	f32 cy = Cos(rot.y);
	f32 sz = Sin(0);
	f32 cz = Cos(0);

	result.m00 = cy * cz;
	result.m01 = -cy * sz;
}

inline Matrix4 Scale(const Vector3 &scale)
{
	Matrix4 result = {};

	result.m00 = scale.x;
	result.m11 = scale.y;
	result.m22 = scale.z;
	result.m33 = 1.0f;

	return result;
}

inline Matrix4 Scale(const Vector2 &scale)
{
	Matrix4 result = {};

	result.m00 = scale.x;
	result.m11 = scale.y;
	result.m22 = 1.0f;
	result.m33 = 1.0f;

	return result;
}

inline f32 Determinant(const Matrix4 &mat)
{
	return mat.m03 * mat.m12 * mat.m21 * mat.m30 - mat.m02 * mat.m13 * mat.m21 * mat.m30 - mat.m03 * mat.m11 * mat.m22 * mat.m30 + mat.m01 * mat.m13 * mat.m22 * mat.m30 +
		mat.m02 * mat.m11 * mat.m23 * mat.m30 - mat.m01 * mat.m12 * mat.m23 * mat.m30 - mat.m03 * mat.m12 * mat.m20 * mat.m31 + mat.m02 * mat.m13 * mat.m20 * mat.m31 +
		mat.m03 * mat.m10 * mat.m22 * mat.m31 - mat.m00 * mat.m13 * mat.m22 * mat.m31 - mat.m02 * mat.m10 * mat.m23 * mat.m31 + mat.m00 * mat.m12 * mat.m23 * mat.m31 +
		mat.m03 * mat.m11 * mat.m20 * mat.m32 - mat.m01 * mat.m13 * mat.m20 * mat.m32 - mat.m03 * mat.m10 * mat.m21 * mat.m32 + mat.m00 * mat.m13 * mat.m21 * mat.m32 +
		mat.m01 * mat.m10 * mat.m23 * mat.m32 - mat.m00 * mat.m11 * mat.m23 * mat.m32 - mat.m02 * mat.m11 * mat.m20 * mat.m33 + mat.m01 * mat.m12 * mat.m20 * mat.m33 +
		mat.m02 * mat.m10 * mat.m21 * mat.m33 - mat.m00 * mat.m12 * mat.m21 * mat.m33 - mat.m01 * mat.m10 * mat.m22 * mat.m33 + mat.m00 * mat.m11 * mat.m22 * mat.m33;
}

inline Matrix4 Ortho(f32 left, f32 right, f32 bottom, f32 top, f32 near, f32 far)
{
	Matrix4 result = {};

	f32 xOrth = 2.0f / (right - left);
	f32 yOrth = 2.0f / (top - bottom);
	//f32 zOrth = 2.0f / (far - near);
	f32 zOrth = 1.0f / (far - near);

	f32 tx = (left + right) / (left - right);
	f32 ty = (bottom + top) / (bottom - top);
	//f32 tz = (near + far) / (near - far);
	f32 tz = near / (near - far);

	result.m00 = xOrth;
	result.m11 = yOrth;
	result.m22 = zOrth;

	result.m30 = tx;
	result.m31 = ty;
	result.m32 = tz;
	result.m33 = 1.0f;

	return result;
}

inline Matrix4 Ortho2D(f32 x, f32 y, f32 width, f32 height)
{
	return Ortho(x, x + width, y, y + height, -1, 1);
}

inline Matrix4 Ortho2D(f32 x, f32 y, f32 width, f32 height, f32 near, f32 far)
{
	return Ortho(x, x + width, y, y + height, near, far);
}

inline Matrix4 LookAt(Vector3 eye, Vector3 at, Vector3 up = { 0.0f, 1.0f, 0.0f })
{
	Matrix4 result = {};

	Vector3 view = Normalized(at - eye);
	Vector3 right = Normalized(Cross(up, view));
	up = Cross(view, right);

	result.m00 = right.x;
	result.m01 = up.x;
	result.m02 = view.x;

	result.m10 = right.y;
	result.m11 = up.y;
	result.m12 = view.y;

	result.m20 = right.z;
	result.m21 = up.z;
	result.m22 = view.z;

	result.m30 = -Dot(right, eye);
	result.m31 = -Dot(up, eye);
	result.m32 = -Dot(view, eye);
	result.m33 = 1.0f;

	return result;
}

inline Matrix4 Transpose(const Matrix4 &a)
{
	Matrix4 result = {};

	result.arr[0] = a.arr[0];
	result.arr[4] = a.arr[1];
	result.arr[8] = a.arr[2];
	result.arr[12] = a.arr[3];
	result.arr[1] = a.arr[4];
	result.arr[5] = a.arr[5];
	result.arr[9] = a.arr[6];
	result.arr[13] = a.arr[7];
	result.arr[2] = a.arr[8];
	result.arr[6] = a.arr[9];
	result.arr[10] = a.arr[10];
	result.arr[14] = a.arr[11];
	result.arr[3] = a.arr[12];
	result.arr[7] = a.arr[13];
	result.arr[11] = a.arr[14];
	result.arr[15] = a.arr[15];

	return result;
}

inline Matrix4 Inverse(const Matrix4 &a)
{
	Matrix4 result = {};

	f32 xx = a.arr[0];
	f32 xy = a.arr[1];
	f32 xz = a.arr[2];
	f32 xw = a.arr[3];
	f32 yx = a.arr[4];
	f32 yy = a.arr[5];
	f32 yz = a.arr[6];
	f32 yw = a.arr[7];
	f32 zx = a.arr[8];
	f32 zy = a.arr[9];
	f32 zz = a.arr[10];
	f32 zw = a.arr[11];
	f32 wx = a.arr[12];
	f32 wy = a.arr[13];
	f32 wz = a.arr[14];
	f32 ww = a.arr[15];

	f32 det = 0.0f;
	det += xx * (yy * (zz * ww - zw * wz) - yz * (zy * ww - zw * wy) + yw * (zy * wz - zz * wy));
	det -= xy * (yx * (zz * ww - zw * wz) - yz * (zx * ww - zw * wx) + yw * (zx * wz - zz * wx));
	det += xz * (yx * (zy * ww - zw * wy) - yy * (zx * ww - zw * wx) + yw * (zx * wy - zy * wx));
	det -= xw * (yx * (zy * wz - zz * wy) - yy * (zx * wz - zz * wx) + yz * (zx * wy - zy * wx));

	f32 invDet = 1.0f / det;

	result.arr[0] = +(yy*(zz*ww - wz*zw) - yz*(zy*ww - wy*zw) + yw*(zy*wz - wy*zz)) * invDet;
	result.arr[1] = -(xy*(zz*ww - wz*zw) - xz*(zy*ww - wy*zw) + xw*(zy*wz - wy*zz)) * invDet;
	result.arr[2] = +(xy*(yz*ww - wz*yw) - xz*(yy*ww - wy*yw) + xw*(yy*wz - wy*yz)) * invDet;
	result.arr[3] = -(xy*(yz*zw - zz*yw) - xz*(yy*zw - zy*yw) + xw*(yy*zz - zy*yz)) * invDet;

	result.arr[4] = -(yx*(zz*ww - wz*zw) - yz*(zx*ww - wx*zw) + yw*(zx*wz - wx*zz)) * invDet;
	result.arr[5] = +(xx*(zz*ww - wz*zw) - xz*(zx*ww - wx*zw) + xw*(zx*wz - wx*zz)) * invDet;
	result.arr[6] = -(xx*(yz*ww - wz*yw) - xz*(yx*ww - wx*yw) + xw*(yx*wz - wx*yz)) * invDet;
	result.arr[7] = +(xx*(yz*zw - zz*yw) - xz*(yx*zw - zx*yw) + xw*(yx*zz - zx*yz)) * invDet;

	result.arr[8] = +(yx*(zy*ww - wy*zw) - yy*(zx*ww - wx*zw) + yw*(zx*wy - wx*zy)) * invDet;
	result.arr[9] = -(xx*(zy*ww - wy*zw) - xy*(zx*ww - wx*zw) + xw*(zx*wy - wx*zy)) * invDet;
	result.arr[10] = +(xx*(yy*ww - wy*yw) - xy*(yx*ww - wx*yw) + xw*(yx*wy - wx*yy)) * invDet;
	result.arr[11] = -(xx*(yy*zw - zy*yw) - xy*(yx*zw - zx*yw) + xw*(yx*zy - zx*yy)) * invDet;

	result.arr[12] = -(yx*(zy*wz - wy*zz) - yy*(zx*wz - wx*zz) + yz*(zx*wy - wx*zy)) * invDet;
	result.arr[13] = +(xx*(zy*wz - wy*zz) - xy*(zx*wz - wx*zz) + xz*(zx*wy - wx*zy)) * invDet;
	result.arr[14] = -(xx*(yy*wz - wy*yz) - xy*(yx*wz - wx*yz) + xz*(yx*wy - wx*yy)) * invDet;
	result.arr[15] = +(xx*(yy*zz - zy*yz) - xy*(yx*zz - zx*yz) + xz*(yx*zy - zx*yy)) * invDet;

	return result;
}

inline Matrix4 FlipProjHandedNess(const Matrix4 &a)
{

}
