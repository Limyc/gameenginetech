#include "ostrich_platform.h"

#undef NEAR
#undef near
#undef FAR
#undef far


#include "ostrich.h"
#include "ostrich_vector2.h"
#include "ostrich_vector3.h"
#include "ostrich_matrix.h"
#include "ostrich_rendering.h"
#include "ostrich_physics.cpp"

internal void AddNullEntity(GameState *state)
{
	Assert(state->entityCount < ArrayCount(state->entities));

	Entity *entity = &state->entities[state->entityCount++];
	*entity = {};
	entity->type = EntityType::Null;
}

internal Entity *AddPlayer(GameState *state, v2 position)
{
	Assert(state->entityCount < ArrayCount(state->entities));

	Entity *entity = &state->entities[state->entityCount++];
	*entity = {};
	entity->type = EntityType::Player;
	entity->size = v2{ 1.0f, 1.0f };
	
	Body *body = &entity->body;
	body->gravityScale = 1.0f;
	body->massData.mass = 5.0;
	body->massData.invMass = 1.0f / entity->body.massData.mass;
	body->drag = 15.0f;
	body->position = position;

	AABB aabb = {};
	aabb.center = v2{ 0, entity->size.y } *0.5f;
	aabb.extents = entity->size * 0.5f;
	body->shape.type = ShapeType::Rect;
	body->shape.aabb = aabb;

	return entity;
}

internal Entity *AddWall(GameState *state, v2 position, v2 size)
{
	Assert(state->entityCount < ArrayCount(state->entities));
	Assert(size.x != 0 && size.y != 0);

	Entity *entity = &state->entities[state->entityCount++];
	*entity = {};
	entity->type = EntityType::Wall;
	entity->size = size;
	entity->transform.position = position;

	AABB aabb = {};
	aabb.center = v2{ 0, 0 };
	aabb.extents = entity->size * 0.5f;
	entity->body.shape.type = ShapeType::Rect;
	entity->body.shape.aabb = aabb;
	entity->body.position = position;

	return entity;
}

internal Entity *AddBox(GameState *state, v2 position, v2 size)
{
	Assert(state->entityCount < ArrayCount(state->entities));

	Entity *entity = &state->entities[state->entityCount++];
	*entity = {};
	entity->type = EntityType::Wall;
	entity->size = size;
	entity->transform.position = position;

	Body *body = &entity->body;
	body->gravityScale = 1.0f;
	body->massData.mass = 1;
	body->massData.invMass = 1.0f / entity->body.massData.mass;
	body->drag = 1.0f;
	body->position = position;

	AABB aabb = {};
	aabb.center = v2{ 0, 0 };
	aabb.extents = entity->size * 0.5f;
	body->shape.type = ShapeType::Rect;
	body->shape.aabb = aabb;

	return entity;
}

//internal void AttachCollider(MemoryArena *arena, Body *body, ShapeType type)
//{
//	body->position = entity->transform.position;
//
//	switch (type)
//	{
//		case ShapeType::Rect:
//			break;
//		case ShapeType::Circle:
//			break;
//		case ShapeType::Poly:
//			break;
//	}
//
//	AABB *aabb = PushStruct(arena, AABB);
//	aabb->center = v2{ 0, 0 };
//	aabb->extents = entity->size * 0.5f;
//	entity->body.shape->type = ShapeType::Rect;
//	entity->body.shape->data = aabb;
//}

internal void MovePlayer(GameInput *input, Entity *player)
{
	f32 dt = input->fixedDeltaTime;

	GameControllerInput *controller = &input->controllers[0];

	b32 useGrav = true;
	v2 accel = {};

	{
		if (controller->moveUp.endedDown)
		{
			accel.y += 1;
			useGrav = false;
		}

		if (controller->moveDown.endedDown)
		{
			accel.y -= 1;
		}

		if (controller->moveLeft.endedDown)
		{
			accel.x -= 1;
		}

		if (controller->moveRight.endedDown)
		{
			accel.x += 1;
		}
	}

	f32 accelLength = LengthSqr(accel);

	if (accelLength > 1.0f)
	{
		accel *= 1.0f / Sqrt(accelLength);
	}

	Body *rb = &player->body;
	//	Transform *tf = &player->transform;

	accel *= 50.0f;
	// damp
	accel += -rb->drag * rb->velocity;
	accel *= rb->massData.invMass;

	// gravity
	if (useGrav)
	{
		accel += k_gravity * rb->gravityScale;
	}

	v2 oldPlayerPos = rb->position;
	v2 playerDelta = (0.5f * accel * (dt * dt)) + rb->velocity * dt;
	rb->velocity = (accel * dt) + rb->velocity;

	v2 desiredPos = oldPlayerPos + playerDelta;

	if (rb->velocity.x < 0)
	{
		player->isFlipped = true;
	}
	else if (rb->velocity.x > 0)
	{
		player->isFlipped = false;
	}

	rb->position = desiredPos;
}

internal void MovePrimative(Entity *entity, f32 dt)
{
	Body *rb = &entity->body;
	//	Transform *tf = &player->transform;

	v2 accel = -rb->drag * rb->velocity;
	accel *= rb->massData.invMass;
	// gravity
	accel += k_gravity * rb->gravityScale;


	v2 oldPos = rb->position;
	v2 delta = (0.5f * accel * (dt * dt)) + rb->velocity * dt;
	rb->velocity = (accel * dt) + rb->velocity;

	v2 desiredPos = oldPos + delta;

	rb->position = desiredPos;
}

//inline v2 WorldToScreen(Camera *cam, v3 screenPos, s32 width, s32 height)
//{
//	v2 result;
//	mat4 viewProj = Translate(cam->transform.position) * cam->projection;
//
//	v3 p = screenPos * viewProj;
//
//	result.x = RoundToInt32(((p.x + 1) / 2.0f) * width);
//	result.y = RoundToInt32(((p.y + 1) / 2.0f) * height);
//
//	return result;
//}

inline v3 ScreenToWorld(Camera *cam, v3 screenPos, s32 width, s32 height)
{
	v3 result = {};

	mat4 viewProj = Inverse(Translate(cam->transform.position) * cam->projection);

	v4 pos;
	pos.x = (2.0f * (screenPos.x / width)) - 1.0f;
	pos.y = 1.0f - (2.0f * (screenPos.y / height));
	pos.z = 0.0f;
	pos.w = 1.0f;

	pos = pos * viewProj;
	pos.w = 1.0f / pos.w;

	for (u32 i = 0; i < 3; ++i)
	{
		result.arr[i] = pos.arr[i] * pos.w;
	}

	return result;
}

extern "C" GAME_SIMULATE(GameSimulate)
{
	GameState *gameState = (GameState *)memory->permStorage.base;
	//GameState *oldGameState = (GameState *)(memory->permStorage.base + sizeof(GameState));

	if (!memory->isInitialized)
	{
		//PushArray(&memory->permStorage, 2, GameState);
		PushStruct(&memory->permStorage, GameState);
		*gameState = {};

		memIndex worldArenaSize = Megabytes(128);

		InitializeArena(&gameState->worldArena,
						worldArenaSize,
						memory->permStorage.base + memory->permStorage.used);

		PushSize(&memory->permStorage, worldArenaSize);

		gameState->camera = {};
		gameState->camera.transform.position = { 160, 90 };
		gameState->camera.projection = Ortho2D(0, 0, 320, 180, -1, 1);

		AddNullEntity(gameState);

		//char *atlasPath = "C:\\Workspace\\CPP\\OstrichKnight\\data\\sprites\\ostrich_knight_1.png";
		char *atlasPath = "..\\data\\sprites\\ostrich_knight_1.png";
		Sprite *atlas = &gameState->ostrichAtlas;
		atlas->pixels = memory->platformApi.LoadPng(atlasPath, &atlas->width, &atlas->height, &atlas->numComponents);


		Entity *player = AddPlayer(gameState, {});

		player->animations = PushArray(&gameState->worldArena, 1, Animation);

		player->animations[0].msPerFrame = 1.0f / 10.0f;
		player->animations[0].nextFrameTime = player->animations[0].msPerFrame;
		player->animations[0].framesCount = 5;
		player->animations[0].frames = PushArray(&gameState->worldArena, player->animations[0].framesCount, v4);

		v4 *frames = player->animations[0].frames;
		u32 currentFrame = 1;
		u32 numWide = 3;

		s32 spriteDimX = 32;
		s32 spriteDimY = 32;

		s32 x = 0;
		s32 y = 0;
		v2 dim = { (f32)spriteDimX / atlas->width, (f32)spriteDimY / atlas->height };

		for (u32 i = 0; i < player->animations[0].framesCount; ++i)
		{
			x = (currentFrame % numWide) * spriteDimX;
			y = (currentFrame / numWide) * spriteDimY;

			Assert(x < atlas->width);
			Assert(y < atlas->height);

			frames[i] = { (f32)x / atlas->width, (f32)y / atlas->height, dim.x, dim.y };

			++currentFrame;
		}

		AddWall(gameState, v2{ 0, -2.0f }, v2{ 5.0f, 1.0f });

		Camera *cam = &gameState->camera;
		cam->projection = Ortho2D(0, 0, 320, 180, -1, 1);
		cam->transform.position = { 160, 90 };

		memory->isInitialized = true;
	}


	if (input->mouseButtons[0].endedDown && !input->mouseButtons[0].wasDown)
	{
		v3 mousePos = v3{ (f32)input->mouseX, (f32)input->mouseY, 0};
		//mousePos /= 4;
		mousePos = ScreenToWorld(&gameState->camera, mousePos, 1280, 720);

		v2 pos = v2{ mousePos.x, mousePos.y };
		pos /= PIXELS_PER_METER;
		AddBox(gameState, pos, v2{ 1, 1 });
	}

	f32 dt = input->fixedDeltaTime;

	for (u32 i = 1; i < gameState->entityCount; ++i)
	{
		Entity *current = &gameState->entities[i];

		for (u32 j = i + 1; j < gameState->entityCount; ++j)
		{
			if (i == j) { continue; }

			Entity *other = &gameState->entities[j];

			Manifold m = {};
			m.a = &current->body;
			m.b = &other->body;

			if (m.a->massData.invMass == 0 && m.b->massData.invMass == 0) { continue; }

			b32 hit = false;

			switch (current->body.shape.type)
			{
				case ShapeType::Rect:
				{
					hit = AABBvsAABB(&m);
				} break;
				case ShapeType::Circle:
				{

				} break;
			}

			if (hit)
			{
				ResolveCollision(&m);
				PositionalCorrection(&m);
			}
		}

		switch (current->type)
		{
			case EntityType::Player:
			{
				MovePlayer(input, current);
			} break;
			case EntityType::Wall:
			{
				MovePrimative(current, input->fixedDeltaTime);
			} break;
		}
	}
}

inline void BatchSprite(SpriteBatchData *batch, v4 *spriteFrame, mat4 *model)
{
	Assert(batch->spriteCount < batch->spriteCountMax);

	batch->spriteFrame[batch->spriteCount] = *spriteFrame;
	batch->model[batch->spriteCount] = *model;
	++batch->spriteCount;
}

inline void BatchPrimative(PrimativeBatchData*batch, mat4 *model)
{
	Assert(batch->primativeCount < batch->primativeCountMax);

	batch->model[batch->primativeCount++] = *model;
}

//internal void InterpolateSimState(GameState *current, GameState *old, f32 alpha)
//{
//	Entity n[256] = {};
//
//	Entity *c = current->entities;
//	Entity *p = old->entities;
//
//	u32 count = old->entityCount;
//
//	for (u32 i = 0; i < count; ++i)
//	{
//		n[i].transform.position = c[i].body.position * alpha + p[i].body.position * (1 - alpha);
//	}
//}

extern "C" GAME_RENDER(GameRender)
{
	renderBuffer->cmdsCount = 0;

	GameState *gameState = (GameState *)memory->permStorage.base;
	//GameState *oldGameState = (GameState *)(memory->permStorage.base + sizeof(GameState));

	Camera *cam = &gameState->camera;

	if (!renderBuffer->isInitialized)
	{
		memIndex renderArenaSize = Megabytes(64);

		InitializeArena(&renderBuffer->arena,
						renderArenaSize,
						memory->permStorage.base + memory->permStorage.used);

		PushSize(&memory->permStorage, renderArenaSize);

		GLfloat textureVerts[] = {
			// Positions         // Texture Coords
			32.0f,	32.0f,  1.0f, 1.0f, // Top Right
			32.0f,	0.0f,   1.0f, 0.0f, // Bottom Right
			0.0f,	0.0f,   0.0f, 0.0f, // Bottom Left
			0.0f,	32.0f,  0.0f, 1.0f, // Top Left 
		};

		GLuint textureIndicies[] =
		{
			0, 1, 3,
			1, 2, 3
		};

		LoadSpriteVaoData *sVAO = PushStruct(&renderBuffer->arena, LoadSpriteVaoData);

		sVAO->quadsCount = 20;
		sVAO->quads = PushArray(&renderBuffer->arena, sVAO->quadsCount, GLfloat);

		for (u32 i = 0; i < sVAO->quadsCount; ++i)
		{
			sVAO->quads[i] = textureVerts[i];
		}

		sVAO->indiciesCount = 6;
		sVAO->indicies = PushArray(&renderBuffer->arena, sVAO->indiciesCount, GLuint);

		for (u32 i = 0; i < sVAO->indiciesCount; ++i)
		{
			sVAO->indicies[i] = textureIndicies[i];
		}

		sVAO->matrixCountMax = 100000;
		sVAO->frameCountMax = 100000;

		sVAO->quadVbo = &renderBuffer->vbos[0];
		sVAO->indexVbo = &renderBuffer->vbos[1];
		sVAO->frameVbo = &renderBuffer->vbos[2];
		sVAO->matrixVbo = &renderBuffer->vbos[3];

		RenderCommand *cmd = &renderBuffer->cmds[renderBuffer->cmdsCount++];
		cmd->data = sVAO;
		cmd->type = CommandType::LoadSpriteVao;

		Sprite *sprite = &gameState->ostrichAtlas;

		LoadSpriteData *loadSpriteData = PushStruct(&renderBuffer->arena, LoadSpriteData);
		*loadSpriteData = {};
		loadSpriteData->textureID = &renderBuffer->tex1;
		loadSpriteData->width = sprite->width;
		loadSpriteData->height = sprite->height;
		loadSpriteData->pixels = sprite->pixels;

		cmd = &renderBuffer->cmds[renderBuffer->cmdsCount++];
		cmd->data = loadSpriteData;
		cmd->type = CommandType::LoadSprite;

		LoadPrimativeVaoData *cVAO = PushStruct(&renderBuffer->arena, LoadPrimativeVaoData);
		cVAO->primativeVbo = &renderBuffer->vbos[4];
		cVAO->matrixVbo = &renderBuffer->vbos[5];
		cVAO->vertCount = 240;
		cVAO->verts = PushArray(&renderBuffer->arena, cVAO->vertCount, GLfloat);
		cVAO->matrixCountMax = 100000;

		for (u32 i = 0; i < cVAO->vertCount; i += 2)
		{
			f32 rads = (TAU32 / 120.0f) * (i / 2);
			f32 radius = (PIXELS_PER_METER / 2);
			f32 x = Cos(rads) * radius;
			f32 y = Sin(rads) * radius;

			cVAO->verts[i] = x;
			cVAO->verts[i + 1] = y;
		}

		cmd = &renderBuffer->cmds[renderBuffer->cmdsCount++];
		cmd->data = cVAO;
		cmd->type = CommandType::LoadCircleVao;

		GLfloat rectVerts[] =
		{
			-16.0f,  16.0f, // Top Left
			 16.0f,  16.0f, // Top Right
			 16.0f, -16.0f, // Bottom Right
			-16.0f, -16.0f, // Bottom Left
		};

		LoadPrimativeVaoData *rVAO = PushStruct(&renderBuffer->arena, LoadPrimativeVaoData);
		rVAO->primativeVbo = &renderBuffer->vbos[6];
		rVAO->matrixVbo = &renderBuffer->vbos[7];
		rVAO->vertCount = 8;
		rVAO->verts = PushArray(&renderBuffer->arena, rVAO->vertCount, GLfloat);
		rVAO->matrixCountMax = 100000;

		for (u32 i = 0; i < rVAO->vertCount; i += 2)
		{
			rVAO->verts[i] = rectVerts[i];
			rVAO->verts[i + 1] = rectVerts[i + 1];

		}

		cmd = &renderBuffer->cmds[renderBuffer->cmdsCount++];
		cmd->data = rVAO;
		cmd->type = CommandType::LoadRectVao;

		renderBuffer->isInitialized = true;
	}
	else
	{
		Clear(&renderBuffer->arena);
	}

	Entity *entities = gameState->entities;

	for (u32 i = 0; i < gameState->entityCount; ++i)
	{
		entities[i].transform.position = entities[i].body.position;
	}

	v2 instMin = { -30, -50 };
	v2 instMax = { 30, 50 };
	u32 instCount = (u32)(instMax.x - instMin.x + 1) * (u32)(instMax.y - instMin.y + 1);;
	RenderCommand *cmd;

	SpriteBatchData *spriteBatch = PushStruct(&renderBuffer->arena, SpriteBatchData);
	*spriteBatch = {};
	spriteBatch->spriteCountMax = instCount;
	spriteBatch->spriteFrame = PushArray(&renderBuffer->arena, spriteBatch->spriteCountMax, v4);
	spriteBatch->model = PushArray(&renderBuffer->arena, spriteBatch->spriteCountMax, mat4);

	mat4 model;
	mat4 view = Translate(cam->transform.position);
	mat4 viewProj = view * cam->projection;
	renderBuffer->viewProj = viewProj;


	PrimativeBatchData *circleBatch = PushStruct(&renderBuffer->arena, PrimativeBatchData);
	*circleBatch = {};
	circleBatch->primativeCountMax = instCount;
	circleBatch->model = PushArray(&renderBuffer->arena, 1, mat4);

	PrimativeBatchData *rectBatch = PushStruct(&renderBuffer->arena, PrimativeBatchData);
	*rectBatch = {};
	rectBatch->primativeCountMax = instCount;
	rectBatch->model = PushArray(&renderBuffer->arena, 1, mat4);

	for (u32 i = 0; i < gameState->entityCount; i++)
	{
		Entity *e = &entities[i];

		switch (e->type)
		{
			case EntityType::Player:
			{
				Animation *anim = &e->animations[0];
				f64 delta = anim->nextFrameTime - input->gameTime;

				if (delta <= 0)
				{
					anim->nextFrameTime = input->gameTime + anim->msPerFrame;
					anim->currentFrame = (anim->currentFrame + 1) % anim->framesCount;
				}

				u32 animOffset = 0;

				if (e->isFlipped)
				{
					v2 size = v2{ -e->size.x, 0 } *0.5f;

					v2 pos = e->transform.position - size;
					model = Translate(pos * PIXELS_PER_METER);
					model = Scale(v2{ -1, 1 }) * model;

					u32 frame = (anim->currentFrame + animOffset++) % anim->framesCount;
					BatchSprite(spriteBatch, &anim->frames[frame], &model);
				}
				else
				{
					v2 size = v2{ e->size.x, 0 } *0.5f;

					v2 pos = e->transform.position - size;
					model = Translate(pos * PIXELS_PER_METER);

					u32 frame = (anim->currentFrame + animOffset++) % anim->framesCount;
					BatchSprite(spriteBatch, &anim->frames[frame], &model);
				}

				// Render AABB
#if 0
				v2 pos = e->transform.position + e->body.shape.aabb.center;
				model = Translate(pos * PIXELS_PER_METER);
				model = Scale(e->body.shape.aabb.extents * 2.0f) * model;
				BatchPrimative(rectBatch, &model);
#endif
			} break;
			case EntityType::Wall:
			{
				v2 pos = e->transform.position;
				model = Translate(pos * PIXELS_PER_METER);
				model = Scale(e->size) * model;

				BatchPrimative(rectBatch, &model);
			} break;
			case EntityType::Ball:
			{
				v2 pos = e->transform.position;
				model = Translate(pos * PIXELS_PER_METER);
				model = Scale(e->size) * model;

				BatchPrimative(circleBatch, &model);
			} break;
			case EntityType::Null:
			{

			} break;
		}
	}

	cmd = &renderBuffer->cmds[renderBuffer->cmdsCount++];
	cmd->type = CommandType::DrawSpriteBatch;
	cmd->data = spriteBatch;

	cmd = &renderBuffer->cmds[renderBuffer->cmdsCount++];
	cmd->type = CommandType::DrawCircleBatch;
	cmd->data = circleBatch;

	cmd = &renderBuffer->cmds[renderBuffer->cmdsCount++];
	cmd->type = CommandType::DrawRectBatch;
	cmd->data = rectBatch;

	//model = Translate(v2{ PIXELS_PER_METER, PIXELS_PER_METER } *-0.5f);

	//DrawRectData *rectData = PushStruct(&renderBuffer->arena, DrawRectData);
	//rectData->model = model;

	//cmd = &renderBuffer->cmds[renderBuffer->cmdsCount++];;
	//cmd->type = CommandType::DrawRect;
	//cmd->data = (void *)rectData;
}

// paul.javid@gmail.com