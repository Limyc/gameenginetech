#include "ostrich_physics.h"
#include "ostrich_vector2.h"

internal b32 TestAABBAABB(AABB a, AABB b)
{
	for (u32 i = 0; i < 2; i++)
	{
		if (Abs(a.center.arr[i] - b.center.arr[i]) > (a.extents.arr[i] + b.extents.arr[i])) return false;
	}

	return true;
}

internal b32 TestCircleCircle(Circle a, Circle b)
{
	f32 distSqr = LengthSqr(a.center - b.center);

	f32 r = a.radius + b.radius;

	return distSqr <= (r * r);
}

internal b32 CircleVsCircle(Manifold *m)
{
	Body *a = m->a;
	Body *b = m->b;

	Assert(a->shape.type == ShapeType::Circle);
	Assert(b->shape.type == ShapeType::Circle);

	Circle sa = a->shape.circle;
	Circle sb = b->shape.circle;

	v2 n = b->position - a->position;

	f32 r = sa.radius + sb.radius;

	f32 dist = LengthSqr(n);

	// Check if circles are even touching
	if (dist > (r * r))
	{
		return false;
	}

	dist = Sqrt(dist);

	if (dist != 0)
	{
		m->penetration = r - dist;
		m->normal = n / dist;
		return true;
	}
	else
	{
		// Circles are in the same position
		// Use arbitrary but consistent values
		m->penetration = sa.radius;
		m->normal = v2{ 1, 0 };
		return true;
	}
}

internal b32 AABBvsAABB(Manifold *m)
{
	Body *a = m->a;
	Body *b = m->b;

	Assert(a->shape.type == ShapeType::Rect);
	Assert(b->shape.type == ShapeType::Rect);

	AABB sa = a->shape.aabb;
	AABB sb = b->shape.aabb;

	v2 n = (b->position + sb.center) - (a->position + sa.center);

	v2 overlap = { sa.extents.x + sb.extents.x - Abs(n.x), 0 };

	// SAT test
	if (overlap.x > 0)
	{
		overlap.y = sa.extents.y + sb.extents.y - Abs(n.y);

		if (overlap.y > 0)
		{
			if (overlap.x < overlap.y)
			{
				if (n.x < 0)
				{
					m->normal = v2{ -1, 0 };
				}
				else
				{
					m->normal = v2{ 1, 0 };
				}

				m->penetration = overlap.x;
				
				return true;
			}
			else
			{
				if (n.y < 0)
				{
					m->normal = v2{ 0, -1 };
				}
				else
				{
					m->normal = v2{0, 1};
				}

				m->penetration = overlap.y;

				return true;
			}
		}
	}

	return false;
}

internal b32 AABBvsCircle(Manifold *m)
{
	Body *a = m->a;
	Body *b = m->b;

	Assert(a->shape.type == ShapeType::Rect);
	Assert(b->shape.type == ShapeType::Circle);

	AABB sa = a->shape.aabb;
	Circle sb = b->shape.circle;

	v2 n = b->position - a->position;
	v2 closest = Clamp(n, -sa.extents, sa.extents);

	b32 isInside = false;
	
	// Clamp circles center to closest AABB edge if inside
	if (n == closest)
	{
		isInside = true;

		if (Abs(n.x) > Abs(n.y))
		{
			closest.x = closest.x > 0 ? sa.extents.x : -sa.extents.x;
		}
		else
		{
			closest.y = closest.y > 0 ? sa.extents.y : -sa.extents.y;
		}
	}

	v2 normal = n - closest;
	f32 dist = LengthSqr(normal);
	f32 r = sb.radius;

	// If the dist between circle center is less than circle raius
	// there is no collision
	if (dist > (r * r) && !isInside)
	{
		return false;
	}

	dist = Sqrt(dist);

	if (isInside)
	{
		m->normal = -n;
		m->penetration = r - dist;
	}
	else
	{
		m->normal = n;
		m->penetration = r - dist;
	}

	return true;
}

internal void ResolveCollision(Manifold *m)
{
	Body *a = m->a;
	Body *b = m->b;

	v2 rv = b->velocity - a->velocity;

	f32 velAlongNormal = Dot(rv, m->normal);

	// Only resolve collisions moving toward each other
	if (velAlongNormal > 0) { return; }

	f32 imA = a->massData.invMass;
	f32 imB = b->massData.invMass;

	// Calculate restitution
	f32 e = Minimum(a->restitution, b->restitution);
	// Calculate impluse
	f32 j = -(1 - e) * velAlongNormal;
	j /= imA + imB;

	v2 impulse = j * m->normal;

	a->velocity -= imA * impulse;
	b->velocity += imB * impulse;
}

internal void PositionalCorrection(Manifold *m)
{
	f32 imA = m->a->massData.invMass;
	f32 imB = m->b->massData.invMass;


	v2 correction = (Maximum(m->penetration - k_slop, 0.0f) / (imA + imB)) 
					* k_correctionPercent * m->normal;

	m->a->position -= imA * correction;
	m->b->position += imB * correction;
}
