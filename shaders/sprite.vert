#version 330 core
layout (location = 0) in vec2 in_position;
layout (location = 1) in vec3 in_color;
layout (location = 2) in vec2 in_texCoord;
layout (location = 3) in vec4 in_spriteFrame;
layout (location = 4) in mat4 in_model;

uniform mat4 viewProj;

out vec2 my_spriteUV;

void main()
{
	gl_Position = viewProj * in_model * vec4(in_position, 0.0f, 1.0f);
	// Invert y-axis
	vec2 uv = vec2(in_texCoord.x, 1.0 - in_texCoord.y);

	my_spriteUV = in_spriteFrame.xy + (uv * in_spriteFrame.zw);
}