#version 330 core

in vec2 my_spriteUV;

out vec4 out_color;

// Texture sampler
uniform sampler2D spriteTexture;

void main()
{
	vec4 texColor = texture(spriteTexture, my_spriteUV);
	
	if(texColor.a < 0.9) { discard; }

	out_color = texColor;
}