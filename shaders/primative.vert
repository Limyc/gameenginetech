#version 330 core
layout (location = 0) in vec2 in_position;
layout (location = 1) in vec3 in_color;
layout (location = 4) in mat4 in_model;

uniform mat4 viewProj;

out vec3 my_color;

void main()
{
	gl_Position = viewProj * in_model * vec4(in_position, 0.0f, 1.0f);

	my_color = vec3(1.0);
}