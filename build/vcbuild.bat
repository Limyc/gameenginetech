:: Arguments 
:: %1 - Output type (exe, dll)
:: %2 - Platform (x64, x86, Win32)
:: %3 - Config (Debug, Release)

@echo off

set ctime=%~dp0ctime.exe
set ctimeOutput=%~dp0ostrich_knight.ctm

%ctime% -begin %ctimeOutput%

setlocal enabledelayedexpansion

if "%1"=="dll" (
call :getBuildSettings "%~2" platform config
) else (
set platform=%2
set config=%3
)

set codePath=%~dp0..\src
set includePath=%~dp0..\external\include
set libraryPath=%~dp0..\external\lib
set outputPath=%~dp0..\bin\%platform%\%config%

:: Dependency paths
set sdl2Path=%libraryPath%\SDL2\%platform%

:: Setup dev environment
set devEnvArgs=%platform%

if "%devEnvArgs%"=="Win32" (
set devEnvArgs=x86
)

if not defined DevEnvVar (
call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" %devEnvArgs%
)

rem set dateTime=%date:~-4,4%%date:~-10,2%%date:~-7,2%_%time:~0,2%%time:~3,2%%time:~6,2%

:: Setup build flags
set dllName=ostrich
set exeName=OstrichKnight

set commonCompilerFlags=-nologo -fp:fast -Gm- -GR- -EHa- -Oi -FC -Zi -WX -W4 -wd4514 -wd4505 -wd4201 -wd4189 -wd4100 -I"%includePath%" 
set commonCompilerFlags=-DOKG_INTERNAL=1 %commonCompilerFlags%
set commonLinkerFlags=-incremental:no /LIBPATH:"%sdl2Path%" SDL2.lib SDL2main.lib

if %config%==Debug (
set commonCompilerFlags=-MTd -Od -DOKG_SLOW=1 %commonCompilerFlags%
) else (
set commonCompilerFlags=-MT -O2 %commonCompilerFlags%
)

set dllCompilerFlags=-Fe"%outputPath%\%dllName%" -Fo"%outputPath%\obj\%dllName%.obj" -Fm"%outputPath%\%dllName%.map" -LD
set exeCompilerFlags=-Fe"%outputPath%\%exeName%" -Fo"%outputPath%\obj\%exeName%.obj" -Fm"%outputPath%\%exeName%.map"
set exeCompilerFlags=-DGLEW_STATIC=1 %exeCompilerFlags%

rem -EXPORT:GameGetSoundSamples 
set dllLinkerFlags=-EXPORT:GameRender -EXPORT:GameSimulate -PDB:"%outputPath%\%dllName%_%random%.pdb"
set exeLinkerFlags=-opt:ref user32.lib gdi32.lib winmm.lib opengl32.lib

if not exist %outputPath% mkdir %outputPath%
pushd %outputPath%

if not exist .\obj mkdir .\obj
if exist *.pdb del *.pdb > nul 2> nul

:: Copy dependency DLLs to output directory
xcopy "%sdl2Path%\SDL2.dll" "%outputPath%\" /d /i /y /f /r

echo WAITING FOR PDB > lock.tmp

:: 32-bit build
rem cl %commonCompilerFlags% %codePath%\win32_ld34.cpp /link %commonLinkerFlags% -subsystem:windows,5.1

:: 64-bit build
cl %commonCompilerFlags% %dllCompilerFlags% %codePath%\ostrich.cpp /link %commonLinkerFlags% %dllLinkerFlags%
del lock.tmp
set LastError=%ERRORLEVEL%
if %1==exe (
cl %commonCompilerFlags% %exeCompilerFlags% %codePath%\win32_ostrich.cpp /link %commonLinkerFlags% %exeLinkerFlags%
)

popd

%ctime% -end %ctimeOutput% %LastError%

goto :EOF

:getBuildSettings <binary_path_in> <platform_out> <config_out>
call :split "%~1" "\" array

set /a c1 = !array.length! - 2
set /a c2 = !array.length! - 1

call set "%~2=%%array[!c1!]%%"
call set "%~3=%%array[!c2!]%%"

goto :EOF

:: split subroutine
:split <string_to_split> <split_delimiter> <array_to_populate>
:: populates <array_to_populate>
:: creates arrayname.length (number of elements in array)
:: creates arrayname.ubound (upper index of array)

set "_data=%~1"

:: replace delimiter with " " and enclose in quotes
set _data="!_data:%~2=" "!"

:: remove empty "" (comment this out if you need to keep empty elements)
set "_data=%_data:""=%"

:: initialize array.length=0, array.ubound=-1
set /a "%~3.length=0, %~3.ubound=-1"

for %%I in (%_data%) do (
    set "%~3[!%~3.length!]=%%~I"
    set /a "%~3.length+=1, %~3.ubound+=1"
)
goto :EOF
